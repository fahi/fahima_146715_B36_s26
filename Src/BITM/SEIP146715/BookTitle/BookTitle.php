<?php
namespace App\BookTitle;
use App\Message\Message;
use App\Utility\Utility;
use App\Model\Database as DB;
use PDO;


class BookTitle extends DB
{
    public $id = "";
    public $book_title = "";
    public $author_name = "";

    public function __construct()
    {
        parent:: __construct();
        if (!isset($_SESSION)) session_start();
    }// end of __construct()


    public function setData($postVariableData=NULL){

        if( array_key_exists("id",$postVariableData) ){

            $this->id     =  $postVariableData['id'];
        }

        if( array_key_exists("book_title",$postVariableData) ){

            $this->book_title     =  $postVariableData['book_title'];
        }

        if( array_key_exists("author_name",$postVariableData) ){

            $this->author_name   =  $postVariableData['author_name'];
        }
    }   // end of setData()



   public function store(){
        $arrData = array($this->book_title,$this->author_name);
        $sql = "insert into book_title(book_title,author_name) VALUES(?,?)";
        $STH = $this->DBH->prepare($sql);
        $result =$STH->execute($arrData );
        Utility::redirect('create.php');

        if($result)
            Message::message("<div id='msg'>Success! Data Has Been Inserted Successfully :)</div>");
        else
            Message::message("<div id='msg'>Failed! Data Has Not Been Inserted Successfully :( </div>");



   }// end of store()




    public function index($fetchMode='ASSOC'){

        $STH = $this->DBH->query('SELECT * from book_title');

        $fetchMode = strtoupper($fetchMode);
        if(substr_count($fetchMode,'OBJ') > 0)
            $STH->setFetchMode(PDO::FETCH_OBJ);
        else
            $STH->setFetchMode(PDO::FETCH_ASSOC);

        $arrAllData  = $STH->fetchAll();
        return $arrAllData;


    }// end of index();






    public function view(){

        $sql = 'SELECT * from book_title where id='.$this->id;

        $STH = $this->DBH->query($sql);


            $STH->setFetchMode(PDO::FETCH_OBJ);

        $arrOneData  = $STH->fetch();
        return $arrOneData;


    }// end of view();



}//  end of BookTitle Class