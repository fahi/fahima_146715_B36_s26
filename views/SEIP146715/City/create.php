<?php
require_once("../../../vendor/autoload.php");
use App\Message\Message;

echo Message::message();



?>


<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <title>City </title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
</head>
<body class="blurBg-true" style="background-color:#EBEBEB">



<!-- Start Formoid form-->
<link rel="stylesheet" href="../../../resource/city_files/formoid1/formoid-flat-black.css" type="text/css" />
<script type="text/javascript" src="../../../resource/city_files/formoid1/jquery.min.js"></script>
<form class="formoid-flat-black"  style="background-color:#ffffff;font-size:15px;font-family:Verdana,Geneva,sans-serif;color:#000000;max-width:480px;min-width:150px" action="store.php" method="post"><div class="title"><h2>City</h2></div>
    <div class="element-input"><label class="title">Enter user name</label><input class="large" type="text" name="input1" /></div>
    <div class="element-multiple"><label class="title">Select user city</label><div class="large"><select data-no-selected="Nothing selected" name="multiple" multiple="multiple" >

                <option value="Dhaka">Dhaka</option>
                <option value="Chittagong">Chittagong</option>
                <option value="Rajshahi">Rajshahi</option>
                <option value="Barishal">Barishal</option>
                <option value="Dinajpur">Dinajpur</option></select></div></div>
    <div class="submit"><input type="submit" value="ADD"/></div></form><p class="frmd"><a href="http://formoid.com/v29.php">bootstrap form</a> Formoid.com 2.9</p><script type="text/javascript" src="../../../resource/city_files/formoid1/formoid-flat-black.js"></script>
<!-- Stop Formoid form-->



</body>
</html>
