<?php
require_once("../../../vendor/autoload.php");
use App\Message\Message;

echo Message::message();



?>

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <title>Profile Picture - Formoid jquery form validation</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
</head>
<body class="blurBg-true" style="background-color:#d7e4eb">



<!-- Start Formoid form-->
<link rel="stylesheet" href="../../../resource/profilepict_files/formoid1/formoid-solid-blue.css" type="text/css" />
<script type="text/javascript" src="../../../resource/profilepict_files/formoid1/jquery.min.js"></script>
<form enctype="multipart/form-data" class="formoid-solid-blue" style="background-color:#FFFFFF;font-size:14px;font-family:'Roboto',Arial,Helvetica,sans-serif;color:#34495E;max-width:480px;min-width:150px"  action="store.php" method="post"><div class="title"><h2>Profile Picture</h2></div>
    <div class="element-input"><label class="title"></label><div class="item-cont"><input class="large" type="text" name="input" placeholder="enter user name"/><span class="icon-place"></span></div></div>
    <div class="element-file"><label class="title"></label><div class="item-cont"><label class="large" ><div class="button">Choose File</div><input type="file" class="file_input" name="file" /><div class="file_text">No file selected</div><span class="icon-place"></span></label></div></div>
    <div class="submit"><input type="submit" value="upload"/></div></form><p class="frmd"><a href="http://formoid.com/v29.php">jquery form validation</a> Formoid.com 2.9</p><script type="text/javascript" src="../../../resource/profilepict_files/formoid1/formoid-solid-blue.js"></script>
<!-- Stop Formoid form-->



</body>
</html>
