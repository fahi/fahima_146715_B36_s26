<?php

define('EMAIL_FOR_REPORTS', '');
define('RECAPTCHA_PRIVATE_KEY', '@privatekey@');
define('FINISH_URI', 'http://');
define('FINISH_ACTION', 'message');
define('FINISH_MESSAGE', 'Thanks for filling out my form');
define('UPLOAD_ALLOWED_FILE_TYPES', 'doc, docx, xls, csv, txt, rtf, html, zip, jpg, jpeg, png, gif');

define('_DIR_', str_replace('\\', '/', dirname(__FILE__)) . '/');
require_once _DIR_ . '/handler.php';

?>

<?php if (frmd_message()): ?>
<link rel="stylesheet" href="<?php echo dirname($form_path); ?>/formoid-flat-black.css" type="text/css" />
<span class="alert alert-success"><?php echo FINISH_MESSAGE; ?></span>
<?php else: ?>
<!-- Start Formoid form-->
<link rel="stylesheet" href="<?php echo dirname($form_path); ?>/formoid-flat-black.css" type="text/css" />
<script type="text/javascript" src="<?php echo dirname($form_path); ?>/jquery.min.js"></script>
<form class="formoid-flat-black" style="background-color:#ffffff;font-size:15px;font-family:Verdana,Geneva,sans-serif;color:#000000;max-width:480px;min-width:150px" method="post"><div class="title"><h2>City</h2></div>
	<div class="element-input<?php frmd_add_class("input1"); ?>"><label class="title">Enter user name</label><input class="large" type="text" name="input1" /></div>
	<div class="element-multiple<?php frmd_add_class("multiple"); ?>"><label class="title">Select user city</label><div class="large"><select data-no-selected="Nothing selected" name="multiple[]" multiple="multiple" >

		<option value="Dhaka">Dhaka</option>
		<option value="Chittagong">Chittagong</option>
		<option value="Rajshahi">Rajshahi</option>
		<option value="Barishal">Barishal</option>
		<option value="Dinajpur">Dinajpur</option></select></div></div>
<div class="submit"><input type="submit" value="ADD"/></div></form><script type="text/javascript" src="<?php echo dirname($form_path); ?>/formoid-flat-black.js"></script>

<!-- Stop Formoid form-->
<?php endif; ?>

<?php frmd_end_form(); ?>